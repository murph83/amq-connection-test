/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements. See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.apache.activemq.artemis.jms.example;

import javax.jms.*;
import javax.naming.InitialContext;
import java.util.Properties;

public class ConnectionTest {

    public static void main(final String[] args) throws Exception {

        String hostname = null;
        String user = null;
        String password = null;

        if (args.length > 2) {
            hostname = args[0];
            user = args[1];
            password = args[2];
        }

        if (hostname == null) {
            hostname = "tcp://localhost:61616";
        }

        if (user == null) {
            user = "bad";
        }

        if (password == null) {
            password = "password";
        }

        Connection failConnection = null;

        Properties props = new Properties();
        props.put("java.naming.factory.initial", "org.apache.activemq.artemis.jndi.ActiveMQInitialContextFactory");
        props.put("connectionFactory.ConnectionFactory", hostname);
        props.put("topic.topic/genericTopic", "genericTopic");

        InitialContext initialContext = null;
        try {
            // /Step 1. Create an initial context to perform the JNDI lookup.
            initialContext = new InitialContext(props);

            // Step 2. perform lookup on the topics
            Topic genericTopic = (Topic) initialContext.lookup("topic/genericTopic");

            // Step 3. perform a lookup on the Connection Factory
            ConnectionFactory cf = (ConnectionFactory) initialContext.lookup("ConnectionFactory");

            // Step 4. Try to create a JMS Connection without user/password. It will fail.
            try {
                failConnection = cf.createConnection(user, password);
            } catch (JMSSecurityException e) {
                System.out.println("Default user cannot get a connection. Details: " + e.getMessage());
            }

        } finally {
            // Be sure to close our JMS resources!
            if (failConnection != null) {
                failConnection.close();
            }

            // Also the initialContext
            if (initialContext != null) {
                initialContext.close();
            }

        }
    }

    private static Connection createConnection(final String username,
                                               final String password,
                                               final ConnectionFactory cf) throws JMSException {
        return cf.createConnection(username, password);
    }
}
